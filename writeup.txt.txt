Our custom objects are a Minecraft Sword, Minecraft Zombies, and a
timer.

The minigame that we added is a timed zombie battle. The player
must draw the sword from the stone, defeat all the zombies, and
return the sword in order to complete the minigame. Upon completion,
the player will recieve a time that they took to complete the tasks.